
module.exports = {
    centerAddress:"0x1c49bec2c753F67116ff91035A80b3aF86c24482",
    privatekey:"0x47e1f97a126ee9408ec200c6d03cfc2b38a4db34120290b62130eaffcffe51c2",
    receiptAddress:"0x220c8F45dcF8026b0D67ecCf3528554A0D94bF74",
    mysql:{
        host: '127.0.0.1',
        user: 'root',
        password: 'root',
        database: 'exchange'
    },
    web3URL:'https://mainnet.infura.io/v3/0e0ad170de4348d4830bb116b0b67c92'
}
