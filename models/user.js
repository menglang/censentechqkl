
let sqlHelpper = require("../utils/sqlHelpper")

module.exports = {

    createUser: async (address, privateKey, mnemonic) => {
        let data = await sqlHelpper.query("insert into users values(0, ?, ?, ?,0)", [address, privateKey, mnemonic])
        return data;
    },
    updateUser: async (status, address) => {
        let sql = "UPDATE users SET status = ? WHERE address = ?"
        let data = await sqlHelpper.query(sql, [status, address])
        return data;
    },

    getUserByStatus: async (status) => {
        let data = await sqlHelpper.query("select address,privateKey from users where status=?", [status])
        return data;
    },

    getUserAll: async () => {
        let data = await sqlHelpper.query("select address,privateKey from users")
        return data;
    }
}