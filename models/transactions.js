let sqlHelpper = require("../utils/sqlHelpper")

module.exports = {

    getTxByHashAndAddress: async (address, hash) => {
        let data = await sqlHelpper.query("select address,hash from transactions where hash= ? and address=?", [hash,address])
        let count = data.data
        return count;
    },
    addTx: async (address, hash) => {
        let data = await sqlHelpper.query("insert into transactions values(0, ?, ?)", [address, hash])
        return data;
    }
}