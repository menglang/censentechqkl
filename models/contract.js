
let web3 = require("../utils/myUtils").getweb3()
let { success, fail } = require("../utils/myUtils")
let BigNumber = require('bignumber.js');

module.exports = {
    //将最小单位的金额转换为最单位的金额
    switchToMaxBalanceUint: async (myContract, balance) => {
        let decimal = await myContract.methods.decimals().call()
        return balance / (Math.pow(10, decimal))
    },

    createContract: async (abi, address) => {
        return new web3.eth.Contract(JSON.parse(abi), address)
    },

    sendTokenTransaction: async (myContract, fromaddress, toaddress, number, privatekey) => {

        let responseData
        var Tx = require('ethereumjs-tx').Transaction;
        var privateKey = Buffer.from(privatekey.slice(2), 'hex')

        let decimals = await myContract.methods.decimals().call()
        let balance = number * Math.pow(10, decimals)
        try {
            let myBalance = await myContract.methods.balanceOf(fromaddress).call()
            let Balance = await web3.utils.hexToNumber(myBalance)

            let ETHbalance = await web3.eth.getBalance(fromaddress)
            ETHbalance = await web3.utils.fromWei(ETHbalance, "ether")
            // let fee = await web3.utils.toBN(ETHbalance)
            var feeNumber = new BigNumber(ETHbalance);
            if (feeNumber < 0.0008) {
                responseData = await fail("ETH不足")
            } else {
                if (Balance < balance) {
                    responseData = await fail("余额不足")
                } else {

                    let nonce = await web3.eth.getTransactionCount(fromaddress)
                    try {
                        let tokenData = await myContract.methods.transfer(toaddress, balance).encodeABI()

                        var rawTx = {
                            nonce: nonce,
                            gasLimit: web3.utils.toHex(80000),
                            gasPrice: web3.utils.toHex(10000000000),
                            to: myContract.options.address,
                            from: fromaddress,
                            data: tokenData
                        }
                    } catch (e) {
                        console.log(e)
                        return responseData = fail("收款地址错误")
                    }
                    var tx = new Tx(rawTx);
                    try {
                        tx.sign(privateKey);
                    } catch (e) {
                        console.log(e)
                        return responseData = fail("私钥错误")

                    }
                    var serializedTx = tx.serialize();

                    let promise = new Promise(async (resolve, reject) => {
                        await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (error, data) {
                            console.log("error50:" + error)
                            if (error) {
                                reject(error)
                            } else {
                                resolve(data)
                            }
                        })
                    })
                    await promise.then(async function (data) {
                        responseData = await success({
                            "transactionHash": data
                        })

                    }, async function (error) {
                        console.log("error64:" + error)
                        responseData = await fail("交易失败")
                    })
                }
            }
        } catch (e) {
            console.log(e)
            return responseData = fail("付款地址错误")
        }

        return responseData
    },

    sendETHTransaction: async (fromaddress, toaddress, number, privatekey) => {
        var Tx = require('ethereumjs-tx').Transaction;
        var privateKey = Buffer.from(privatekey.slice(2), 'hex')
        let nonce = await web3.eth.getTransactionCount(fromaddress)
        let balance = web3.utils.toWei(number)

        var rawTx = {
            nonce: web3.utils.toHex(nonce++),
            gasPrice: web3.utils.toHex(10000000000),
            gasLimit: web3.utils.toHex(50000),
            to: toaddress,
            value: web3.utils.toHex(balance),
            data: '0x00'//转ｔｏｋｅｎ会用到的一个字段
        }

        var tx = new Tx(rawTx);
        tx.sign(privateKey);

        var serializedTx = tx.serialize().toString('hex');

        let responseData;
        let promise = new Promise(async (resolve, reject) => {
            await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), async function (err, data) {

                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
        await promise.then(async function (data) {
            console.log("104data:" + JSON.stringify(data))
            responseData = await success({
                "transactionHash": data,
                "address": fromaddress
            })

        })

        return responseData;
    },

    sendETHFeeTransaction: async (fromaddress, toaddress, number, privatekey, nonce) => {
        var Tx = require('ethereumjs-tx').Transaction;
        var privateKey = Buffer.from(privatekey.slice(2), 'hex')
        // let nonce = await web3.eth.getTransactionCount(fromaddress)
        let balance = web3.utils.toWei(number)

        var rawTx = {
            nonce: web3.utils.toHex(nonce),
            gasPrice: web3.utils.toHex(10000000000),
            gasLimit: web3.utils.toHex(50000),
            to: toaddress,
            value: web3.utils.toHex(balance),
            data: '0x00'//转ｔｏｋｅｎ会用到的一个字段
        }

        var tx = new Tx(rawTx);
        tx.sign(privateKey);

        var serializedTx = tx.serialize().toString('hex');

        let responseData;
        let promise = new Promise(async (resolve, reject) => {
            await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), async function (err, data) {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
        await promise.then(async function (data) {
            console.log("ETHTxHash:" + JSON.stringify(data))
            responseData = await success({
                "transactionHash": data,
                "address": fromaddress
            })

        })

        return responseData;
    },


    sendUSDTTransaction: async (myContract, fromaddress, toaddress, number, privatekey) => {

        var Tx = require('ethereumjs-tx').Transaction;
        var privateKey = Buffer.from(privatekey.slice(2), 'hex')

        let nonce = await web3.eth.getTransactionCount(fromaddress)

        let tokenData = await myContract.methods.transfer(toaddress, number).encodeABI()

        var rawTx = {
            nonce: nonce,
            gasLimit: web3.utils.toHex(50000),
            gasPrice: web3.utils.toHex(10000000000),
            to: myContract.options.address,
            from: fromaddress,
            data: tokenData
        }

        var tx = new Tx(rawTx);
        tx.sign(privateKey);

        var serializedTx = tx.serialize();

        let responseData;
        let promise = new Promise(async (resolve, reject) => {
            await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (error, data) {
                if (error) {
                    reject(error)
                } else {
                    resolve(data)
                }
            })
        })
        await promise.then(async function (data) {
            console.log("USDTTxHash:" + data)
            responseData = await success({
                "transactionHash": data
            })

        }, async function (error) {
            responseData = await fail("失败")
        })

        return responseData
    }
}