
let Contract = require("../models/contract")
var gene = require("../config/gene");

let transactions = require("../models/transactions")
let web3 = require("../utils/myUtils").getweb3()
let { success, fail } = require("../utils/myUtils")
var request = require('request');


async function getGENEBalance(contract, balance) {
    let decimal = await contract.methods.decimals().call()
    return balance / (Math.pow(10, decimal))
}


module.exports = {
    //GENE提现
    cash: async (ctx) => {
        let { from, to, num, privateKey } = ctx.request.body
        let contract = await Contract.createContract(gene.abi, gene.address)
        //通过智能合约对象从中心账户发起转账
        let resData = await Contract.sendTokenTransaction(contract, from, to, parseFloat(num), privateKey)

        ctx.body = resData

    },

    recharge: async (ctx) => {
        let address = ctx.request.body.address
        if (address.trim() == "") {
            ctx.body = fail("参数异常")
            return
        }
        let tAddress = await web3.utils.toChecksumAddress(address)
        let list = []
        let endBlockNumber = await web3.eth.getBlockNumber();
        let startBlockNumber = endBlockNumber - 200;
        let promise = new Promise(async (resolve, reject) => {
            await request({
                url: 'https://api.etherscan.io/api?module=account&'
                    + 'action=tokentx&'
                    + 'contractaddress=0xbb7C85DEE0074a00Bd0C13c85Abe2ecbc7779914&'
                    + 'startblock=' + startBlockNumber + '&'
                    + 'sort=desc&'
                    + 'apikey=73D66KP2HBDVQZRXJWZDG4IMMKJ6TUGZ2S&'
                    + 'page=1&offset=200&'
                    + 'address=' + tAddress,
                method: 'GET',
                headers: { 'Content-Type': 'text/json' }
            }, async function (error, data) {

                if (!error) {
                    resolve(data)

                } else {
                    reject(err)
                }
            })
        })
        await promise.then(async function (data) {
            // if(JSON.parse(data.body).status == '0'){
            //     ctx.body = fail("参数异常");
            //     return 
            // }
            let result = JSON.parse(data.body).result

            for (var i = 0; i < result.length; i++) {
                let flag = result[i]

                let toAddress = await web3.utils.toChecksumAddress(flag.to)
                flag.to = toAddress
                if (toAddress == tAddress) {
                    let count = await transactions.getTxByHashAndAddress(toAddress, flag.hash)

                    if (count.length == 0) {
                        await transactions.addTx(toAddress, flag.hash)
                        list.push(flag)
                    }
                }
            }

            ctx.body = success(list);

        })
    },

    getGENEBalance: async (ctx) => {
        let { address } = ctx.request.body
        let contract = await Contract.createContract(gene.abi, gene.address)
        let balance = await contract.methods.balanceOf(address).call()
        let Balance = await getGENEBalance(contract, balance)
        ctx.body = success({ "balance": Balance })
    }
}
