
let Contract = require("../models/contract")
var usdt = require("../config/usdt");
let config = require("../config/config")

let ethers = require('ethers');
let user = require("../models/user")
let transactions = require("../models/transactions")
let web3 = require("../utils/myUtils").getweb3()
let { success, fail } = require("../utils/myUtils")
var request = require('request');
var schedule = require('node-schedule');
let myUtils = require("../utils/myUtils")

async function scheduleCollet() {
    await schedule.scheduleJob('0 10 15 * * *', async function () {
        await sendTransaction()
    });
    await schedule.scheduleJob('0 15 15 * * *', async function () {
        await sendUSDTTransaction()
    });
}

async function sendTransaction() {
    let { data } = await user.getUserAll()
    let contract = await Contract.createContract(usdt.abi, usdt.address)
    let nonce = await web3.eth.getTransactionCount(config.centerAddress)
    for (let i = 0; i < data.length; i++) {
        let record = data[i]
        let balance = await contract.methods.balanceOf(record.address).call()
        let Balance = await web3.utils.hexToNumber(balance)
        if (Balance > 1000000) {
            let resData = Contract.sendETHFeeTransaction(config.centerAddress, record.address, "0.0005", config.privatekey, nonce)
            nonce++
        } else {
            continue
        }

    }
    console.log(myUtils.timestampToLocal() + "ETH手续费转账完成")
}

async function sendUSDTTransaction() {
    let { data } = await user.getUserAll()
    let contract = await Contract.createContract(usdt.abi, usdt.address)
    for (let i = 0; i < data.length; i++) {
        let record = data[i]
        let balance = await contract.methods.balanceOf(record.address).call()
        let value = await web3.utils.hexToNumberString(balance)
        let Balance = await web3.utils.hexToNumber(balance)
        if (Balance > 1000000) {
            //通过智能合约对象从中心账户发起转账
            let resData = await Contract.sendUSDTTransaction(contract, record.address, config.receiptAddress, value, record.privateKey)
        } else {
            continue
        }
    }
    console.log(myUtils.timestampToLocal() + "USDT归集完成")
}

async function getETHBalance(address) {
    let balance = await web3.eth.getBalance(address)
    return web3.utils.fromWei(balance, "ether")
}

async function getUSDTBalance(contract, balance) {
    let decimal = await contract.methods.decimals().call()
    return balance / (Math.pow(10, decimal))
}


module.exports = {
    //USDT提现
    cash: async (ctx) => {
        let { from, to, num, privateKey } = ctx.request.body
        console.log("privateKey"+privateKey)
        let contract = await Contract.createContract(usdt.abi, usdt.address)
        //通过智能合约对象从中心账户发起转账
        let resData = await Contract.sendTokenTransaction(contract, from, to, parseFloat(num), privateKey)

        ctx.body = resData

    },

    recharge: async (ctx) => {
        let address = ctx.request.body.address
        if (address.trim() == "") {
            ctx.body = fail("参数异常")
            return
        }
        let tAddress = await web3.utils.toChecksumAddress(address)
        let list = []
        let endBlockNumber = await web3.eth.getBlockNumber();
        let startBlockNumber = endBlockNumber - 200;
        let promise = new Promise(async (resolve, reject) => {
            await request({
                url: 'https://api.etherscan.io/api?module=account&'
                    + 'action=tokentx&'
                    + 'contractaddress=0xdAC17F958D2ee523a2206206994597C13D831ec7&'
                    + 'startblock=' + startBlockNumber + '&'
                    + 'sort=desc&'
                    + 'apikey=73D66KP2HBDVQZRXJWZDG4IMMKJ6TUGZ2S&'
                    + 'page=1&offset=200&'
                    + 'address=' + tAddress,
                method: 'GET',
                headers: { 'Content-Type': 'text/json' }
            }, async function (error, data) {

                if (!error) {
                    resolve(data)

                } else {
                    reject(err)
                }
            })
        })
        await promise.then(async function (data) {
            // if(JSON.parse(data.body).status == '0'){
            //     ctx.body = fail("参数异常");
            //     return 
            // }
            let result = JSON.parse(data.body).result

            for (var i = 0; i < result.length; i++) {
                let flag = result[i]

                let toAddress = await web3.utils.toChecksumAddress(flag.to)
                flag.to = toAddress
                if (toAddress == tAddress) {
                    let count = await transactions.getTxByHashAndAddress(toAddress, flag.hash)

                    if (count.length == 0) {
                        await transactions.addTx(toAddress, flag.hash)
                        list.push(flag)
                    }
                }
            }

            ctx.body = success(list);

        })
    },

    getTransaction: async (ctx) => {
        let tranHash = ctx.request.body.tranHash
        if (tranHash.trim() == "") {
            ctx.body = fail("参数错误")
            return
        }
        let data = await web3.eth.getTransactionReceipt(tranHash)
        if(data == null){
            ctx.body = success("")
        }else if (data.status) {
            ctx.body = success({ "transaction": data })
        } else {
            ctx.body = fail({ "transaction": data })
        }

    },

    //新建ETH賬號
    newEthAccount: async (ctx) => {
        //生成新的助记词、私钥、地址
        let wallet = ethers.Wallet.createRandom();
        let mnemonic = wallet.mnemonic;
        let address = wallet.address;
        let privateKey = wallet.privateKey;

        let tAddress = await web3.utils.toChecksumAddress(address)
        data = await user.createUser(tAddress, privateKey, mnemonic)
        if (data.error) {
            ctx.body = fail("注册失败")
            return
        }
        ctx.body = success({
            "address": tAddress,
        })
    },
    //(归集)ETH转账
    sendTransaction: async (ctx) => {
        let { data } = await user.getUserAll()
        let contract = await Contract.createContract(usdt.abi, usdt.address)
        let nonce = await web3.eth.getTransactionCount(config.centerAddress)
        for (let i = 0; i < data.length; i++) {
            let record = data[i]
            let balance = await contract.methods.balanceOf(record.address).call()
            let Balance = await web3.utils.hexToNumber(balance)
            if (Balance > 1000000) {
                let resData = Contract.sendETHFeeTransaction(config.centerAddress, record.address, "0.0005", config.privatekey, nonce)
                nonce++
            } else {
                continue
            }

        }
        ctx.body = success("手续费已转入")

    },

    // （归集）USDT转入中心账号
    sendUSDTTransaction: async (ctx) => {
        let { data } = await user.getUserAll()
        let contract = await Contract.createContract(usdt.abi, usdt.address)
        for (let i = 0; i < data.length; i++) {
            let record = data[i]
            let balance = await contract.methods.balanceOf(record.address).call()
            let value = await web3.utils.hexToNumberString(balance)
            let Balance = await web3.utils.hexToNumber(balance)
            if (Balance > 1000000) {
                //通过智能合约对象从中心账户发起转账
                let resData = await Contract.sendUSDTTransaction(contract, record.address, config.receiptAddress, value, record.privateKey)
            } else {
                continue
            }
        }
        ctx.body = success("归集已完成")
    },

    // (归集测试)ETH转账
    // sendTransaction: async (ctx) => {
    //     let address = ctx.request.body.address
    //     let contract = await Contract.createContract(usdt.abi, usdt.address)
    //     let balance = await contract.methods.balanceOf(address).call()
    //     let Balance = await web3.utils.hexToNumber(balance)
    //     console.log("balance:"+Balance)
    //     if (Balance > 0) {
    //         let resData = await Contract.sendETHTransaction(config.centerAddress, address, "0.0002", config.privatekey)
    //         if (resData.code == 0) {
    //             ctx.body = success("手续费已转入")
    //         } else {
    //             ctx.body = fail("失败")
    //             return
    //         }
    //     }
    // },

    // （归集测试）USDT转入中心账号
    // sendUSDTTransaction: async (ctx) => {
    //     let { address, privateKey } = ctx.request.body
    //     let contract = await Contract.createContract(usdt.abi, usdt.address)
    //     console.log("1:")
    //     let balance = await contract.methods.balanceOf(address).call()
    //     let Balance = await web3.utils.hexToNumberString(balance)
    //     console.log("balance:" + balance)
    //     //通过智能合约对象从中心账户发起转账
    //     let resData = await Contract.sendUSDTTransaction(contract, address, config.receiptAddress, Balance, privateKey)
    //     console.log("4:")
    //     if (resData.code == 0) {
    //         ctx.body = success("归集已完成")
    //     } else {
    //         ctx.body = resData
    //         return
    //     }
    // },

    schedule: async () => {
        await scheduleCollet()
    },

    getETHBalance: async (ctx) => {
        let { address } = ctx.request.body
        let balance = await getETHBalance(address)
        ctx.body = success({ "balance": balance })
    },

    getUSDTBalance: async (ctx) => {
        let { address } = ctx.request.body
        let contract = await Contract.createContract(usdt.abi, usdt.address)
        let balance = await contract.methods.balanceOf(address).call()
        let Balance = await getUSDTBalance(contract, balance)
        ctx.body = success({ "balance": Balance })
    },

    checkAddress: async (ctx) => {
        let address = ctx.request.body.address;
        if (address.trim() == "") {
            ctx.body = fail("参数错误")
            return
        }
        let flag = await web3.utils.checkAddressChecksum(address);
        if(flag){
            ctx.body = success("地址正确")
        }else{
            ctx.body = fail("地址错误")
        }
    }
}
