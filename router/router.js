let router = require("koa-router")()

let rechargeController = require("../controllers/rechargeController")

router.post("/USDTCash",rechargeController.cash)

router.post("/USDTAccount",rechargeController.newEthAccount)

router.post("/USDTTx",rechargeController.getTransaction)

router.post("/USDTRecharge",rechargeController.recharge)

router.post("/sendTransaction",rechargeController.sendTransaction)

router.post("/sendUSDTTransaction",rechargeController.sendUSDTTransaction)

router.post("/schedule",rechargeController.schedule)

router.post("/getETHBalance",rechargeController.getETHBalance)

router.post("/getUSDTBalance",rechargeController.getUSDTBalance)

router.post("/checkAddress",rechargeController.checkAddress)


module.exports = router